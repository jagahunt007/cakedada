<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!doctype html>
<!--[if IE 7]>    <html class="ie7" > <![endif]-->
<!--[if IE 8]>    <html class="ie8" > <![endif]-->
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 10]>    <html class="ie10" > <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-US"> <!--<![endif]-->
		<head>
				<!-- META TAGS -->
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width" />
				
				<!-- Title -->
				<title>Cake Dada</title>

                <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,600,800' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'>

				<!-- Style Sheet-->
                <link rel="stylesheet" href="cakedada/css/tooltipster.css">
                <link href="cakedada/css/ie.css" rel="stylesheet" media="all">
                <link rel="stylesheet" href="cakedada/css/bootstrap.css">
                <link rel="stylesheet" href="cakedada/sitecss/style.css">
				<link rel="stylesheet" href="cakedada/css/responsive.css">
                <link rel="stylesheet" href="cakedada/css/prettyPhoto.css">
				<link rel="stylesheet" href="cakedada/css/font-awesome.min.css">

				
				<!-- favicon -->
				<link rel="shortcut icon" href="cakedada/images/cake1.png">

            <!-- Include the HTML5 shiv print polyfill for Internet Explorer browsers 8 and below -->
            <!--[if lt IE 10]><script src="js/html5shiv-printshiv.js" media="all"></script><![endif]-->
		</head>
		<body>		
<div id="preloader"></div>		
				<!-- HEADER -->
				<div class="header-bar">
                    <div class="container">
                        <div class="row">
                            <div class="pric-icon span2">
                              <!--   <a href="#" class="active">&#x20ac;</a>
                                <a href="#">&#xa3;</a>
                                <a href="#">&#36;</a> -->
                            </div>

                            <div class="span10 right">
                                <div class="social-strip">
                                    <ul>
									<li><i class="fa fa-sign-in" aria-hidden="true" style="color: #f71919;font-size: 18px;padding-left: 15px;"></i><a href="signup.html" style="padding-left: 10px;">Sign In/ Sign up</a></li>
                                        <li><a href="#" class="account">My Account</a></li>
                                    <!--     <li><a href="#" class="wish">Wish List</a></li> -->
                                        <li><a href="checkout.html" class="check">Checkout</a></li>
                                    </ul>
                                </div><!-- 

                                <div class="languages">
                                    <a href="#" class="english active"><img src="images/english.png" alt=""></a>
                                    <a href="#" class="german"><img src="images/german.png" alt=""></a>
                                    <a href="#" class="japan"><img src="images/japan.png" alt=""></a>
                                    <a href="#" class="turkish"><img src="images/turkish.png" alt=""></a>
                                </div> -->
                            </div>
                        </div>
                    </div>
				</div>

                <div class="header-top">
                    <div class="container">
                        <div class="row">

                            <div class="span4">
                                 <div class="logo">
                                   <!--  <a href="index.html"><img src="images/cakes/cakedada.png" alt=""></a> -->
                                    <h1 style="font-family:cursive;font-size: 38.5px;font-weight:600"><a href="index.html">Cake<img src="https://res.cloudinary.com/do06pamkd/image/upload/v1492060183/logos/cake1.png" width="12%"/>Dada</a></h1>
                                </div>
                            </div>

                            <div class="span5">
                                <form>
                                    <input type="text" placeholder="Type and hit enter">
                                    <input type="submit" value="">
                                </form>
                            </div>

                            <div class="span3">
                                <div class="cart">
                                    <ul style="float: right;">
                                        <li class="first"><a href="shopping-cart.html"></a><span></span></li>
                                        <li>0 item(s) - <i class="fa fa-inr"></i> 0.00</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                 <header>
                    <div class="container">
                        <div class="row">
                            <div class="span12">
                                <nav class="desktop-nav">
                                    <ul class="clearfix">
                            <li class="active">
                                            <a href="index.html">Home</a>
											</li>
											<li >
                                            <a href="biriyani.html">Biryani</a>
											</li>
                                        <li>
                                            <a href="cake.html">Cakes</a>
                                            <ul class="clearfix sub-menu menu-two">
                                                <li class="clearfix">
                                                    <div class="links">
                                                        <h3>Flavours</h3>
                                                        <p style="font-size: 13px;">
                                                            <a href="#">Vanila</a>
                                                            <a href="#">Butterscotch</a>
                                                            <a href="#">Chocolate</a>
															</p>
															<p style="font-size: 13px;">
                                                            <a href="#">Black Forest</a>
															<a href="#">Choco Vanila</a>
                                                            <a href="#">Chocolate Truffle</a>
                                                           
                                                        </p>

                                                    </div>
                                                    <figure>
                                                        <a href="#"><img src="https://res.cloudinary.com/do06pamkd/image/upload/cakes/O7MF0Q0.jpg" alt="" width="25%"/></a>
                                                    </figure>
                                                </li>
                                            </ul>

                                        </li>
										        <li>
                                            <a href="flowerBouquets.html">Flower Bouquets</a>
                                            <ul class="clearfix sub-menu menu-two">
                                                <li class="clearfix">
                                                    <div class="links">
                                                        <h3>Categories</h3>
                                                        <p>
                                                            <a href="#">Roses</a>
                                                            <a href="#">Lilly</a>
                                                            <a href="#">Gerberas</a>
                                  
                                                           
                                                        </p>

                                                    </div>
                                                    <figure>
                                                        <a href="#"><img src="https://res.cloudinary.com/do06pamkd/image/upload/v1492060183/cakes/9_lillyFlatBouquet.jpg" alt="" width="25%"/></a>
                                                    </figure>
                                                </li>
                                            </ul>

                                        </li>
										
										 <li class="">
                                            <a href="sweets.html">Sweets</a>
											</li>
											<li class="">
                                            <a href="chocolates.html">Chocolates</a>
											</li>
											
											 <li>
                                            <a href="dryfruits.html">Dry Fruits</a>
											</li>
               
                                       <!--  <li>
                                            <a href="#">Pages</a>
                                            <ul class="clearfix">
                                                <li><a href="index.html">Home</a></li>
                                                <li><a href="cake.html">Cakes</a></li>

                                                <li><a href="shopping-cart.html">Shopping cart</a></li>
                                                <li><a href="checkout.html">Checkout</a></li>
                                                <li><a href="single-product.html">Single Product</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="single.html">Single</a></li>
                                            </ul>
                                        </li> -->
                                    </ul>
                                </nav>

                                <select id="mobileMenu">
								 <option value="biriyani.html">Biriyani</option>
                                    <option value="cake.html">Cakes</option>
                                    <option value="flowerBouquets.html">Flower Bouquets</option>
                                    <option value="sweets.html">Sweets </option>
									 <option value="chocolates.html">Chocolates</option>
                                    <option value="dryfruits.html">Dry Fruits</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                </header>
				<!-- HEADER -->
<tiles:insertAttribute name="body" />


                <!-- FOOTER -->
                <div class="shipping-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="span12">
                                <div class="shipping">
                                    <p><span>FREE SHIPPING </span> Offered by Cake Dada - lorem ipsum dolor sit amet mauris accumsan vitate odio tellus</p>
                                    <a href="#" class="button">Learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-wrap">
                    <div class="container">
                        <div class="row">

                            <div class="footer clearfix">

                                <div class="span3">
                                    <div class="widget">
                                        <h3>Customer Service</h3>
                                        <ul>
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Delivery Information</a></li>
                                            <li><a href="#">Privacy Policy</a></li>
                                            <li><a href="#">Terms & Conditions</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="span3">
                                    <div class="widget">
                                        <h3>Information</h3>
                                        <ul>
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Delivery Information</a></li>
                                            <li><a href="#">Privacy Policy</a></li>
                                            <li><a href="#">Terms & Conditions</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="span3">
                                    <div class="widget">
                                        <h3>My Account</h3>
                                        <ul>
                                            <li><a href="#">My Account</a></li>
                                            <li><a href="#">Order History</a></li>
                                            
                                            <li><a href="#">Newsletter</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="span3">
                                    <div class="widget">
                                        <h3>Contact us</h3>
                                        <ul>
                                           <!--  <li>support@maxshop.com</li>
                                            <li>+38649 123 456 789 00</li> -->
                                            <li>Lorem ipsum address street no 24 b41</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <footer class="clearfix">
                                <div class="span5">
                                    <p>� 2017 Planet e software solutions Design, All Rights Reserved</p>
                                </div>
                                <div class="span2 back-top">
                                    <a href="#"> <img src="cakedada/images/back.png" alt=""></a>
                                </div>
                                <div class="span5">
                                    <div class="social-icon">
                                        <a class="rss" href=""></a>
                                        <a class="twet" href=""></a>
                                        <a class="fb" href=""></a>
                                        <a class="google" href=""></a>
                                        <a class="pin" href=""> </a>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                <!-- FOOTER -->

				
				<!-- Scripts -->
				<script src="cakedada/js/jquery-1.9.1.min.js"></script>
                <script src="cakedada/js/jquery-ui.js"></script>
                <script src="cakedada/js/jquery.cycle.all.js"></script>
                <script src="cakedada/js/modernizr.custom.17475.js"></script>
                <script src="cakedada/js/jquery.elastislide.js"></script>
                <script src="cakedada/js/jquery.carouFredSel-6.0.4-packed.js"></script>
                <script src="cakedada/js/jquery.selectBox.js"></script>
                <script src="cakedada/js/jquery.tooltipster.min.js"></script>
                <script src="cakedada/js/jquery.prettyPhoto.js"></script>
				<script src="cakedada/js/sitecustom.js"></script>	
			<script>
				$(document).ready(function(){
				
				$('#mobileMenu').change(function(){
				window.location.href = this.value;
				});
				
				
				});	
			</script>				
		</body>
</html>