package com.cakedada.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AdminController {
	
	
	
	@RequestMapping("/administrator")
	public String adminIndex(){
		
		
		return "adminLogin";
	}
	
	@RequestMapping("/adminLogging")
	public String adminLogging(){
		
		
		return "redirect:adminDashboard";
	}
	
	@RequestMapping("/adminDashboard")
	public String adminDashboard(){
		
		
		return "adminIndex";
	}


}
